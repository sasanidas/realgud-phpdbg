;; Copyright (C) 2016-2017, 2019 Free Software Foundation, Inc

;; Author: Sean Farley <sean@farley.io>, Rocky Bernstein (rocky@gnu.org)

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.or/licenses/>.


(require 'comint)
(require 'realgud)
(require 'load-relative)

(declare-function realgud-lang-mode? 'realgud-lang)
(declare-function realgud-parse-command-arg 'realgud-core)
(declare-function realgud-query-cmdline 'realgud-core)
(declare-function realgud-suggest-invocation 'realgud-core)
(declare-function realgud-get-cmdbuf   'realgud-buffer-helper)

;; FIXME: I think the following could be generalized and moved to
;; realgud-... probably via a macro.
(defvar realgud--phpdbg-minibuffer-history nil
  "minibuffer history list for the command `phpdbg'.")

(defvar realgud--phpdbg-remote-minibuffer-history nil
  "minibuffer history list for the command `phpdbg-remote'.")

(easy-mmode-defmap phpdbg-minibuffer-local-map
  '(("\C-i" . comint-dynamic-complete-filename))
  "Keymap for minibuffer prompting of debugger startup command."
  :inherit minibuffer-local-map)

;; FIXME: I think this code and the keymaps and history
;; variable chould be generalized, perhaps via a macro.
(defun phpdbg-query-cmdline (&optional opt-debugger)
  (realgud-query-cmdline
   'phpdbg-suggest-invocation
   phpdbg-minibuffer-local-map
   'realgud--phpdbg-minibuffer-history
   opt-debugger))

;; phpdbg -q /home/fermin/Programming/phpunit-simple-example/vendor/bin/phpunit /home/fermin/Programming/phpunit-simple-example/tests/AverageTest.php
(defun phpdbg-parse-cmd-args (orig-args)
  ""
  (message "%s" orig-args)
  (let (
	(args orig-args)
	(pair)          ;; temp return from
	(phpdbg-opt-two-args '())
	;; Phpdbg doesn't have mandatory 2-arg options in our sense,
	;; since the two args can be run together, e.g. "-C/tmp" or "-C /tmp"
	;;
	(phpdbg-two-args '())
	;; ipdb doesn't have any arguments
	(interp-regexp
	 "^phpdbg$")

	;; Things returned
	(annotate-p nil)
	(debugger-args '())
	(debugger-name nil)
	(interpreter-args '())
	(script-args '())
	(script-name nil))
    (if (not (and args))
	;; Got nothing: return '(nil, nil)
	(list interpreter-args debugger-args script-args annotate-p)
      ;; else
      ;; Strip off optional "phpdbg" or "phpdbg182" etc.
      (when (string-match interp-regexp
			  (file-name-sans-extension
			   (file-name-nondirectory (car args))))
	(setq interpreter-args (list (pop args)))

	;; Strip off Phpdbg-specific options
	(while (and args
		    (string-match "^-" (car args)))
	  (setq pair (realgud-parse-command-arg
		      args phpdbg-two-args phpdbg-opt-two-args))
	  (nconc interpreter-args (car pair))
	  (setq args (cadr pair))))

      (setq debugger-name (file-name-sans-extension
			   (file-name-nondirectory (car args))))
      (setq debugger-args (list (pop args)))
      ;; Skip to the first non-option argument.
      (while (and args (not script-name))
	(let ((arg (car args)))
	  (cond
	   ;; Options with arguments.
	   ((string-match "^-" arg)
	    (setq pair (realgud-parse-command-arg
			args phpdbg-two-args phpdbg-opt-two-args))
	    (nconc debugger-args (car pair))
	    (setq args (cadr pair)))
	   ;; Anything else must be the script to debug.
	   (t (setq script-name (expand-file-name arg))
	      (setq script-args (cons script-name (cdr args))))
	   )))
      (list interpreter-args debugger-args script-args annotate-p))))


(defun phpdbg-suggest-invocation (debugger-name)
  "Suggest a phpdbg command invocation via `realgud-suggest-invocaton'."
  (realgud-suggest-invocation (or realgud:phpdbg-command-name debugger-name)
			      realgud--phpdbg-minibuffer-history
			      "phpdbg -q -c" "\\.php"))

(defun phpdbg-reset ()
  "Phpdbg cleanup - remove debugger's internal buffers (frame,breakpoints, etc.)."
  (interactive)
  ;; (phpdbg-breakpoint-remove-all-icons)
  (dolist (buffer (buffer-list))
    (when (string-match "\\*phpdbg-[a-z]+\\*" (buffer-name buffer))
      (let ((w (get-buffer-window buffer)))
        (when w
          (delete-window w)))
      (kill-buffer buffer))))

(defun realgud--phpdbg-customize ()
  "Use `customize' to edit the settings of the `phpdbg' debugger."
  (interactive)
  (customize-group 'realgud--phpdbg))

(provide-me "realgud:phpdbg-")
