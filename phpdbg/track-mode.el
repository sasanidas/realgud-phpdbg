;; Copyright (C) 2016, 2018, 2019 Free Software Foundation, Inc

;; Author: Rocky Bernstein <rocky@gnu.org>
;; Author: Sean Farley <sean@farley.io>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;; Python "phpdbg" Debugger tracking a comint buffer.

(eval-when-compile (require 'cl-lib))

(require 'realgud)
(require 'load-relative)

(require-relative-list '("core" "init") "realgud:phpdbg-")

(realgud-track-mode-vars "realgud:phpdbg")

(declare-function realgud-track-mode 'realgud-track-mode)
(declare-function realgud-track-mode-setup 'realgud-track-mode)
(declare-function realgud:track-mode-hook 'realgud-track-mode)
(declare-function realgud:track-set-debugger 'realgud-track-mode)


(defun realgud:phpdbg-track-mode-hook()
  (if realgud:phpdbg-track-mode
      (progn
	(use-local-map realgud:phpdbg-track-mode-map)
	(realgud-track-mode-setup 't)
	(message "using phpdbg mode map"))
    (message "phpdbg track-mode-hook disable called")
    )
  )
(define-minor-mode realgud:phpdbg-track-mode
  "Minor mode for tracking phpdbg source locations inside a process shell via realgud.
If called interactively with no prefix argument, the mode is toggled. A prefix argument, captured as ARG, enables the mode if the argument is positive, and disables it otherwise.

a process shell.

\\{phpdbg-track-mode-map}
"
  :init-value nil
  :global nil
  :group 'realgud:phpdbg
  :keymap realgud:phpdbg-track-mode-map
  (realgud:track-set-debugger "realgud:phpdbg")
  (if realgud:phpdbg-track-mode
      (progn
	(realgud-track-mode-setup 't)
	(realgud:phpdbg-track-mode-hook))
    (progn
      (setq realgud-track-mode nil)
      ))
  )

(provide-me "realgud:phpdbg-")
